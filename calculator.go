package sscalc

import (
	"errors"
	"math"
	"strconv"
)

var (
	prior  =  map[string]uint8{"(": 0, ")": 0, "+": 1, "-": 1, "*": 2, "/": 2, "^": 3}
)

//	Calculate
func Calc(input string) (float64, error) {
	result := make([]float64, 0, len(input))
	stack := make([]string, 0, len(input))
	prior = map[string]uint8{"(": 0, ")": 0, "+": 1, "-": 1, "*": 2, "/": 2, "^": 3}

	if err := process(input, &result, &stack); err != nil {
		return 0, errors.New("invalid process operation: " + err.Error())
	}
	for i := len(stack) - 1; i >= 0; i = i - 1 {
		if err := done(stack[i], &result); err != nil {
			return 0, errors.New("invalid operation: " + err.Error())
		}
	}
	if 0 == len(result) {
		return 0, errors.New("no result")
	} else {
		return result[len(result)-1], nil
	}
}

func done(lexeme interface{}, result *[]float64) (err error) {
	var one, two float64

	//	Panic catcher for unknown error
	defer func() {
		if r := recover(); r != nil {
			err = errors.New(r.(string))
		}
	}()
	switch lexeme.(type) {
	case float64:
		*result = append(*result, lexeme.(float64))
	default:
		one, two, *result = (*result)[len(*result)-2], (*result)[len(*result)-1], (*result)[:len(*result)-2]
		switch lexeme.(string) {
		case "+":
			*result = append(*result, one+two)
		case "-":
			*result = append(*result, one-two)
		case "*":
			*result = append(*result, one*two)
		case "/":
			if two == 0.0 {
				return errors.New("divide by zero")
			}
			*result = append(*result, one/two)
		case "^":
			*result = append(*result, math.Pow(one, two))
		}
	}
	return nil
}

func sortFacility(lexeme interface{}, result *[]float64, stack *[]string) error {
	switch lexeme.(type) {
	case float64:
		if err := done(lexeme.(float64), result); err != nil {
			return errors.New("invalid operation: " + err.Error())
		}
	default:
		token := lexeme.(string)
		switch token {
		case "(":
			*stack = append(*stack, token)
		case "+", "-", "*", "/", "^", ")":
			for i := len(*stack) - 1; i >= 0; i = i - 1 {

				//	Если это закрывающая скобка, а на стеке открывающая, то просто удаляем открывающую со стека и прерываемся
				if (*stack)[i] == "(" && token == ")" {
					*stack = (*stack)[:i]
					break
				}

				//	Если приоритет операции на стеке выше, чем текущая, то текущий элемент со стека выталкиваем в результат
				if prior[(*stack)[i]] >= prior[token] {
					if err := done((*stack)[i], result); err != nil {
						return errors.New("invalid operation: " + err.Error())
					}
					*stack = (*stack)[:i]
				} else {
					break
				}
			}

			//	Если это не закрывающая скобка, то добавляем её на стек
			if token != ")" {
				*stack = append(*stack, token)
			}
		}
	}
	return nil
}

//	Разложение входящей строки на лексемы
//	Каждая полученная лексема отправляется на сортировку
func process(input string, result *[]float64, stack *[]string) error {
	accumulator := make([]byte, 0)
	isNumeric := false
	previous := byte('+')

	for _, value := range []byte(input) {
		switch value {

		//	Убираем лишние символы
		case ' ', '\t':
			continue

		//	Обработка операций
		case '+', '-', '*', '/', '(', ')', '^':

			//	Если в аккумуляторе число, то отправляем его на сортировку
			if isNumeric {
				if number, err := strconv.ParseFloat(string(accumulator), 64); err == nil {
					if err := sortFacility(number, result, stack); err != nil {
						return errors.New("unable to processed: " + err.Error())
					}
				} else {
					return errors.New("unable convert string to float64: " + string(accumulator))
				}
				accumulator = []byte{}
			} else if value == '-' {

				//	А если в аккумуляторе не число, и текущий символ минус, то проверяем, не унарный ли это минус
				if previous == '+' || previous == '-' || previous == '*' || previous == '/' || previous == '(' || previous == '^' {
					accumulator = append(accumulator, value)
					isNumeric = true
					continue
				}
			}

			//	Если минус не унарный, то отправляем лексему операции на сортировку
			if err := sortFacility(string(value), result, stack); err != nil {
				return errors.New("unable to processed: " + err.Error())
			}
			isNumeric = false
		default:

			//	Все числа (а должны остаться только они), добавляем в аккумулятор
			accumulator = append(accumulator, value)
			isNumeric = true
		}
		previous = value
	}

	//	Если после обработки в аккумуляторе осталось число, то отправляем его на сортировку
	if isNumeric {
		if number, err := strconv.ParseFloat(string(accumulator), 64); err == nil {
			if err := sortFacility(number, result, stack); err != nil {
				return errors.New("unable to processed: " + err.Error())
			}
		} else {
			return errors.New("unable convert string to float64: " + string(accumulator))
		}
	}
	return nil
}
